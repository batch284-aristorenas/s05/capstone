USE classic_models;


-- 1.
SELECT customers.customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT products.productName, products.MSRP FROM products WHERE productName = "The Titanic";

-- 4.
SELECT employees.firstName, employees.lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customers.customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT employees.firstName, employees.lastName, employees.email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8.
SELECT orders.customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9.
SELECT productlines.productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11.
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customers.customerName, customers.country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13.
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.city = "Tokyo";

-- 14.
SELECT customers.customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";

-- 15.
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNUmber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports";

-- 16.
-- SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
-- 	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
-- 	JOIN offices ON customers.country = offices.country
-- 	WHERE customers.country = offices.country;

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	WHERE customers.country = offices.country;

-- 17.
SELECT products.productName, products.quantityInStock FROM products 
	JOIN productlines ON products.productLine = productlines.productLine
	WHERE products.productLine = "Planes" AND products.quantityInStock < 1000;

-- 18.
SELECT customers.customerName, customers.phone FROM customers WHERE phone LIKE "+81%";


